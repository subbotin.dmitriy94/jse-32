package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(@NotNull E entity) throws AbstractException;

    void addAll(@NotNull List<E> entities);

    void remove(@NotNull E entity) throws AbstractException;

    void clear();

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator);

    @NotNull
    E findById(@NotNull String id) throws AbstractException;

    @NotNull
    E findByIndex(int index) throws AbstractException;

    void removeById(@NotNull String id) throws AbstractException;

    void removeByIndex(int index) throws AbstractException;

}