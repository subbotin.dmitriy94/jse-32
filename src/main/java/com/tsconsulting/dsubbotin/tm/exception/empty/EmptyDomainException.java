package com.tsconsulting.dsubbotin.tm.exception.empty;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class EmptyDomainException extends AbstractException {

    public EmptyDomainException() {
        super("Empty domain loaded.");
    }

}
