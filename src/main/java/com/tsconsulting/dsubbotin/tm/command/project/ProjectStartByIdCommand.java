package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-start-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Start project by id.";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        serviceLocator.getProjectService().startById(currentUserId, id);
        TerminalUtil.printMessage("[Project started]");
    }

}
