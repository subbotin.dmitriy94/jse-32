package com.tsconsulting.dsubbotin.tm.command.data.binary;

import com.tsconsulting.dsubbotin.tm.command.data.AbstractDataCommand;
import com.tsconsulting.dsubbotin.tm.dto.Domain;
import org.jetbrains.annotations.NotNull;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;

public final class DataBinLoadCommand extends AbstractDataCommand {

    @Override
    public @NotNull String name() {
        return "data-load-bin";
    }

    @Override
    public @NotNull String description() {
        return "Load binary data.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final InputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

}
