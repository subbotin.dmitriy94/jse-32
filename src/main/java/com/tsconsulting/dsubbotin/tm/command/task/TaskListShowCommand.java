package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownSortException;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;

public final class TaskListShowCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-list";
    }

    @Override
    @NotNull
    public String description() {
        return "Display task list.";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter sort:");
        TerminalUtil.printMessage(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        @Nullable List<Task> tasks;
        try {
            @NotNull Sort sortType = EnumerationUtil.parseSort(sort);
            TerminalUtil.printMessage(sortType.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(currentUserId, sortType.getComparator());
        } catch (UnknownSortException e) {
            tasks = serviceLocator.getTaskService().findAll(currentUserId);
        }
        int index = 1;
        for (@NotNull final Task task : tasks) TerminalUtil.printMessage(index++ + ". " + task);
    }

}
