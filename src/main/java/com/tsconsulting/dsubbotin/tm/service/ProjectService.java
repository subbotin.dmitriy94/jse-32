package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIndexException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        @NotNull final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkName(name);
        if (EmptyUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.add(userId, project);
    }

    @Override
    @NotNull
    public Project findByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        return projectRepository.findByName(userId, name);
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkId(id);
        checkName(name);
        projectRepository.updateById(userId, id, name, description);
    }

    @Override
    public void updateByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        if (index < 0) throw new EmptyIndexException();
        checkName(name);
        projectRepository.updateByIndex(userId, index, name, description);
    }

    @Override
    public void startById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        checkId(id);
        projectRepository.startById(userId, id);
    }

    @Override
    public void startByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        checkIndex(index);
        projectRepository.startByIndex(userId, index);
    }

    @Override
    public void startByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        projectRepository.startByName(userId, name);
    }

    @Override
    public void finishById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        checkId(id);
        projectRepository.finishById(userId, id);
    }

    @Override
    public void finishByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        checkIndex(index);
        projectRepository.finishByIndex(userId, index);
    }

    @Override
    public void finishByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        projectRepository.finishByName(userId, name);
    }

    @Override
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws AbstractException {
        checkId(id);
        projectRepository.updateStatusById(userId, id, status);
    }

    @Override
    public void updateStatusByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final Status status
    ) throws AbstractException {
        checkIndex(index);
        projectRepository.updateStatusByIndex(userId, index, status);
    }

    @Override
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws AbstractException {
        checkName(name);
        projectRepository.updateStatusByName(userId, name, status);
    }

    private void checkId(@NotNull final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkIndex(final int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
    }

    private void checkName(@NotNull final String name) throws EmptyNameException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
    }

}
